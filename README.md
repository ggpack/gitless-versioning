# Gitless Versioning

Retrieve versioning information (tag or branch) without installing `git`.

Simple portable scripts for ci-cd operations.

## 🎉 No more git install in ci-cd steps

😢 In the past, developpers needed to install git for each steps requiring to access `git tags` or they needed to share the information extracted in one initial job via artifacts...

 🥳🥂 `gitless-versioning` ✨ provides a solution by giving simple git utilities writen in shell that can be executed on-the-fly by all the steps requiring git information (like docker build).


## Installation
Simply copy / vendor the needed scripts into your ci-cd and call them.

## Usage
🏷️ Getting the latest tag on the current commit:

```
latestTag=$(ci-cd/currentTag.sh)
```

🌵 Getting the current branch:
```
currentBranch=$(ci-cd/currentBranch.sh)
```
It is empty in `detached mode`.

## Roadmap
We are going to vendor the tool on production projects and evaluate the need for additional scripts.
