# Credits to https://gitlab.com/ggpack/gitless-versioning
# Retrieves the current branch for HEAD, empty if no branch (detached mode).
# Must be executed from the project root folder.

(
	cd .git
	# HEAD can contain a simple "sha" (detached mode) or "ref: refs/heads/<branch>"
	if grep -q "ref: refs/heads/" HEAD; then
		basename "$(cat HEAD)"
	fi
)
